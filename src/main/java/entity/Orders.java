package entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Orders {
    @Id
    private Integer Id;
    @Column(name="client_id")
    private long clientId;
    @Column(name="delivery_address")
    private String deliveryAddress;
    @Column(name="final_price")
    private double finalPrice;
    @Column(name="paying_methode")
    @Enumerated(value = EnumType.STRING)
    private PayingMethode payingMethode;
}
