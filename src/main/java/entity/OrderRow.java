package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderRow {
    @Id
    private long id;
    @Column(name = "product_id")
    private long productId;
    private int quantity;
    @Column(name = "final_price")
    private double finalPrice;
    @Column(name = "order_id")
    private int oderId;

}
