package entity;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name= "user_feedback")
public class UserFeedback {
    @Id
    private Integer id;
    @Enumerated(value = EnumType.STRING) //se asigura ca in baza de date se salveaza GOOD sau BAD si nu cifrele 1 sau 2
    private Rating rating;
    @Column(name = "feedback_description")
    private String feedbackDescription;

    @Override
    public String toString() {
        return "UserFeedback{" +
                "id=" + id +
                ", rating=" + rating +
                ", feedbackDescription='" + feedbackDescription + '\'' +
                '}';
    }
}
