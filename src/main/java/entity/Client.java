package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Entity
@AllArgsConstructor
@Data
@NoArgsConstructor
@Getter
@Setter
public class Client {
    @Id
    private Integer id;
    @Column(name="last_name", nullable = false)
    private String lastName;
    @Column(name="first_name", nullable = false)
    private String firstName;
    @Column(nullable = false, unique = true) // CTRL + space pentru a gasi astea
    private String email;
    @Column(name="phone_number")
    private int phoneNumber;
    private String address;

}
