package entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    private Integer id;
    @Column(name= "product_type")
    @Enumerated(value = EnumType.STRING)
    private ProductType productType;
    @Column(nullable = false)
    private String name;
    private String description;
    private Integer quantity;
    @Column(name="buying_price")
    private Double buyingPrice;
    @Column(name="selling_price")
    private double sellingPrice;
    @Column(name="buying_date")
    private LocalDate buyingDate;
    @Column(name="expiration_date")
    private LocalDate expirationDate;

}
