import config.DatabaseConfig;
import entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import repository.*;

import java.time.LocalDate;
import java.util.List;

import static entity.ProductType.NATURAL_FLOWERS;

public class Main {
    public static SupplierRepository supplierRepository = new SupplierRepository(DatabaseConfig.getSessionFactory());
    public static ProductRepository productRepository = new ProductRepository(DatabaseConfig.getSessionFactory());
    public static ClientRepository clientRepository = new ClientRepository(DatabaseConfig.getSessionFactory());
    public static OrderRepository orderRepository = new OrderRepository(DatabaseConfig.getSessionFactory());
    public static OrderRowRepository orderRowRepository = new OrderRowRepository(DatabaseConfig.getSessionFactory());
    public static ServicesRepository servicesRepository = new ServicesRepository(DatabaseConfig.getSessionFactory());
    public static UserFeedbackRepository userFeedbackRepository = new UserFeedbackRepository(DatabaseConfig.getSessionFactory());

    public static void main(String[] args) {
        Supplier s1 = new Supplier(1, "Pepiniere Marioara", "Brasov - Romania");
        Supplier s2 = new Supplier(2, "Depozitul de lalele", "Olanda");
        supplierRepository.save(s1);
        supplierRepository.save(s2);

        Client c1 = new Client(1, "Victor", "Beca", "beca.victor@yahoo.com", 764477172, "Bucuresti");
        Client c2 = new Client(2, "Maria", "Diana", "diana.maria@yahoo.com", 764323421, "China");
        clientRepository.save(c1);
        clientRepository.save(c2);

        Product p1 = new Product(1, NATURAL_FLOWERS, "Lalele", "rosii", 9, 36.23
                , 60.00, LocalDate.of(2023, 6, 18), LocalDate.of(2023, 6, 27));
        productRepository.save(p1);

        Orders o1 = new Orders(1, 1, "Valea Cascadelor, Bucuresti", 80, PayingMethode.CARD);
        orderRepository.save(o1);

        OrderRow Or1 = new OrderRow(1, 1, 8, 80.00, 1);
        orderRowRepository.save(Or1);

        UserFeedback u1 = new UserFeedback(1, Rating.GOOD, "Super");
        UserFeedback u2 = new UserFeedback(2, Rating.GOOD, "Superrr");
        userFeedbackRepository.save(u1);
        userFeedbackRepository.save(u2);

        Services serv1 = new Services(1, "Serviciu", 21.00, "Details");
        servicesRepository.save(serv1);

        u1.setFeedbackDescription("Ne a placut foarte mult floraria");
        userFeedbackRepository.updateUserFeedback(u1);

        System.out.println(userFeedbackRepository.getAll());

        userFeedbackRepository.deleteUserFeedback(u2);

        System.out.println(userFeedbackRepository.getAll());

        displayAllClients();
        displayAllProducts();


    }

    public static void displayAllClients() {
        List<Client> clients = clientRepository.getAll();
        for (Client c : clients) {
            System.out.println("Nume: " + c.getLastName() + " Prenume: " + c.getFirstName() + " Email: " + c.getEmail());
        }
    }

    public static void displayAllProducts() {
        List<Product> products = productRepository.getAll();
        for (Product p : products) {
            System.out.println(p.getName() + " " + p.getDescription() + " " + p.getQuantity()
                    + " bucati " + "cumparati la data de "
                    + p.getBuyingDate() + " cu pretul de " + p.getBuyingPrice() + " valabil pana la "
                    + p.getExpirationDate() + " disponibili la pretul de " + p.getSellingPrice());
        }
    }

}
