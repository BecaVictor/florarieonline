package repository;

import entity.Services;
import entity.UserFeedback;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class UserFeedbackRepository {
    private final SessionFactory sessionFactory;

    public UserFeedbackRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    public void save (UserFeedback userFeedback){
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(userFeedback);
        transaction.commit();

        session.close();
    }
    public List<UserFeedback> getAll(){
        List<UserFeedback> userFeedback = new ArrayList<>();
        Session session = sessionFactory.openSession();
        userFeedback=session.createQuery("SELECT u FROM UserFeedback u", UserFeedback.class).getResultList();
        session.close();return userFeedback;
    }
    public void updateUserFeedback(UserFeedback userFeedback){
        Session s = sessionFactory.openSession();
        Transaction t = s.beginTransaction();
        s.merge(userFeedback);
        t.commit();
        s.close();

    }

    public void deleteUserFeedback (UserFeedback userFeedback){
        Session s = sessionFactory.openSession();
        Transaction t = s.beginTransaction();
        s.remove(userFeedback);
        t.commit();
        s.close();
    }

    @Override
    public String toString() {
        return "UserFeedbackRepository{" +
                "sessionFactory=" + sessionFactory +
                '}';
    }
}
