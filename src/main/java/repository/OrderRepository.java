package repository;

import entity.Orders;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class OrderRepository {
    private final SessionFactory sessionFactory;


    public OrderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save (Orders orders){
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(orders);
        transaction.commit();

        session.close();
    }
}
