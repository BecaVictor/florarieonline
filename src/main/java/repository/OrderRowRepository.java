package repository;

import entity.OrderRow;
import entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class OrderRowRepository {
    private final SessionFactory sessionFactory;


    public OrderRowRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save (OrderRow orderRow){
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(orderRow);
        transaction.commit();

        session.close();
    }
}
