package repository;

import entity.Supplier;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SupplierRepository {
    private final SessionFactory sessionFactory;

    public SupplierRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    public void save (Supplier supplier){
        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();
        session.persist(supplier);
        transaction.commit();

        session.close();
    }


}
