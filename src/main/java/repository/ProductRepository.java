package repository;

import entity.Client;
import entity.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ProductRepository {
    private final SessionFactory sessionFactory;
    public ProductRepository(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    public void save (Product product){
        Session session = sessionFactory.openSession(); // deschide o sesiune de comunicare cu baza de date

        Transaction transaction = session.beginTransaction(); // incepe o tranzactie
        session.persist(product); // avem o singura modificare de facut si anume salvarea product ului
        transaction.commit(); // salveaza in baza de date modificarile facute dupa deschiderea tranzactiei

        session.close(); //inchide sesiunea
    }
    public List<Product> getAll(){
        List<Product> products = new ArrayList<>();
        Session session = sessionFactory.openSession();
        products=session.createQuery("SELECT p FROM Product p", Product.class).getResultList();
        session.close();return products;
    }
}
